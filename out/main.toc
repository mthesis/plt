\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Top Tagging }{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Neuronal Networks }{7}{0}{1}
\beamer@subsectionintoc {1}{3}{Autoencoder }{11}{0}{1}
\beamer@subsectionintoc {1}{4}{Graphs }{15}{0}{1}
\beamer@subsectionintoc {1}{5}{Graph Autoencoder }{19}{0}{1}
\beamer@subsectionintoc {1}{6}{Setup }{24}{0}{1}
\beamer@sectionintoc {2}{Results}{26}{0}{2}
\beamer@subsectionintoc {2}{1}{Problems when scaling }{29}{0}{2}
\beamer@subsectionintoc {2}{2}{Problems by triviality }{32}{0}{2}
\beamer@sectionintoc {3}{Solutions}{34}{0}{3}
\beamer@subsectionintoc {3}{1}{Normalization }{34}{0}{3}
\beamer@subsectionintoc {3}{2}{OneOff networks }{40}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{46}{0}{4}
\beamer@subsectionintoc {4}{1}{other Datasets }{46}{0}{4}
\beamer@subsectionintoc {4}{2}{Conclusion }{49}{0}{4}
\beamer@subsectionintoc {4}{3}{Outlook }{51}{0}{4}
\beamer@sectionintoc {5}{Backup}{52}{1}{5}
\beamer@subsectionintoc {5}{1}{Normalization }{52}{1}{5}
\beamer@subsectionintoc {5}{2}{Datasetups }{53}{1}{5}
\beamer@subsectionintoc {5}{3}{C addition }{56}{1}{5}
